﻿// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT License.

using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using AdaptiveCardsBot.Models;
using Microsoft.Bot.Builder;
using Microsoft.Bot.Schema;
using Newtonsoft.Json;
using System.Linq;

namespace Microsoft.BotBuilderSamples
{
    // This bot will respond to the user's input with an Adaptive Card.
    // Adaptive Cards are a way for developers to exchange card content
    // in a common and consistent way. A simple open card format enables
    // an ecosystem of shared tooling, seamless integration between apps,
    // and native cross-platform performance on any device.
    // For each user interaction, an instance of this class is created and the OnTurnAsync method is called.
    // This is a Transient lifetime service. Transient lifetime services are created
    // each time they're requested. For each Activity received, a new instance of this
    // class is created. Objects that are expensive to construct, or have a lifetime
    // beyond the single turn, should be carefully managed.

    public class AdaptiveCardsBot : ActivityHandler 
    {
        private static string WelcomeText = Environment.NewLine+"Please enter country name to get COVID infection status."+Environment.NewLine+" Please refer this link for country names."+Environment.NewLine+"https://www.worldometers.info/geography/alphabetical-list-of-countries/";

        //This array contains the file location of our adaptive cards
        private readonly string[] _cards =
        {
            Path.Combine(".", "Resources", "FlightItineraryCard.json"),
            Path.Combine(".", "Resources", "ImageGalleryCard.json"),
            Path.Combine(".", "Resources", "LargeWeatherCard.json"),
            Path.Combine(".", "Resources", "RestaurantCard.json"),
            Path.Combine(".", "Resources", "SolitaireCard.json"),
        };

        protected override async Task OnMembersAddedAsync(IList<ChannelAccount> membersAdded, ITurnContext<IConversationUpdateActivity> turnContext, CancellationToken cancellationToken)
        {
            await SendWelcomeMessageAsync(turnContext, cancellationToken);
        }
        protected override async Task OnMessageActivityAsync(ITurnContext<IMessageActivity> turnContext, CancellationToken cancellationToken)
        {
            using (var client = new HttpClient())
            {
                try
                {
                    client.BaseAddress = new Uri("http://40.71.202.138");
                    client.DefaultRequestHeaders.Accept.Clear();
                    var httpResponse = client.GetAsync("/covid/").Result;
                    httpResponse.EnsureSuccessStatusCode();
                    var actualData = await httpResponse.Content.ReadAsStringAsync();

                    httpResponse = client.GetAsync("/timeSeries/confirmed").Result;
                    httpResponse.EnsureSuccessStatusCode();
                    var confirmedData = await httpResponse.Content.ReadAsStringAsync();
                    var confirmedDesData = JsonConvert.DeserializeObject<IEnumerable<Dictionary<string,string>>>(confirmedData);

                    httpResponse = client.GetAsync("/timeSeries/recovered").Result;
                    httpResponse.EnsureSuccessStatusCode();
                    var recoveredData = await httpResponse.Content.ReadAsStringAsync();
                    var recoveredDesData = JsonConvert.DeserializeObject<IEnumerable<Dictionary<string, string>>>(recoveredData);

                    httpResponse = client.GetAsync("/timeSeries/deaths").Result;
                    httpResponse.EnsureSuccessStatusCode();
                    var deathsData = await httpResponse.Content.ReadAsStringAsync();
                    var deathsDesData = JsonConvert.DeserializeObject<IEnumerable<Dictionary<string, string>>>(deathsData);



                    var data = JsonConvert.DeserializeObject<IEnumerable<Covid>>(actualData);
                    string userInput = turnContext.Activity.Text;
                    if (!string.IsNullOrWhiteSpace(userInput) ) {
                        if (data.Count() > 0) {
                            userInput = userInput.ToLower();
                            var countryData = data.Where(x=>x.country.ToLower()==userInput).FirstOrDefault();
                            if (countryData != null)
                            {
                                var countryConfirmed = confirmedDesData.Where(x => x["Country"].ToLower() == userInput).Select(x => x).FirstOrDefault();
                                var countryRecovered = recoveredDesData.Where(x => x["Country"].ToLower() == userInput).Select(x => x).FirstOrDefault();
                                var countryDeaths = deathsDesData.Where(x => x["Country"].ToLower() == userInput).Select(x => x).FirstOrDefault();

                                int confirmedToday, confirmedYesterday;
                                int deathToday, deathsYesterday;
                                int recoveredtoday, recoveredYesterday;
                                var coviddata = new CovidModel();
                                if (countryConfirmed != null)
                                {
                                    var keys = countryConfirmed.Values.ToList();
                                    confirmedToday = Convert.ToInt32(keys[keys.Count-1]);
                                    confirmedYesterday = Convert.ToInt32(keys[keys.Count - 2]);
                                    coviddata.cDiff = confirmedToday - confirmedYesterday;
                                }
                                if (countryRecovered != null)
                                {
                                    var keys = countryRecovered.Values.ToList();
                                    recoveredtoday = Convert.ToInt32(keys[keys.Count - 1]);
                                    recoveredYesterday = Convert.ToInt32(keys[keys.Count - 2]);
                                    coviddata.rDiff = recoveredtoday - recoveredYesterday;
                                }
                                if (countryDeaths != null)
                                {
                                    var keys = countryDeaths.Values.ToList();
                                    deathToday = Convert.ToInt32(keys[keys.Count - 1]);
                                    deathsYesterday = Convert.ToInt32(keys[keys.Count - 2]);
                                    coviddata.dDiff = deathToday - deathsYesterday;
                                }

                                //Random ra = new Random();
                                var card = CreateAdaptiveCardAttachment(_cards[3], countryData,coviddata);

                                //turnContext.Activity.Attachments = new List<Attachment>() { cardAttachment };
                                await turnContext.SendActivityAsync(MessageFactory.Attachment(card), cancellationToken);
                                await turnContext.SendActivityAsync(MessageFactory.Text("Try another country."), cancellationToken);
                            }
                            else {
                                await turnContext.SendActivityAsync(MessageFactory.Text("Details not found,Try another country."), cancellationToken);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    await turnContext.SendActivityAsync(MessageFactory.Text("Please try again"), cancellationToken);

                }
            }
            //Random r = new Random();
            //var cardAttachment = CreateAdaptiveCardAttachment(_cards[r.Next(_cards.Length)]);

            ////turnContext.Activity.Attachments = new List<Attachment>() { cardAttachment };
            //await turnContext.SendActivityAsync(MessageFactory.Attachment(cardAttachment), cancellationToken);
            //await turnContext.SendActivityAsync(MessageFactory.Text("Please enter any text to see another card."), cancellationToken);
        }

        private static async Task SendWelcomeMessageAsync(ITurnContext turnContext, CancellationToken cancellationToken)
        {
            foreach (var member in turnContext.Activity.MembersAdded)
            {
                if (member.Id != turnContext.Activity.Recipient.Id)
                {
                    await turnContext.SendActivityAsync(
                        $"Welcome to COVID status bot. {WelcomeText}",
                        cancellationToken: cancellationToken);
                }
            }
        }
   
        private static Attachment CreateAdaptiveCardAttachment(string filePath,Covid data,CovidModel model)
        {
            var adaptiveCardJson = File.ReadAllText(filePath);
            adaptiveCardJson=adaptiveCardJson.Replace("##CountryName##",data.country);
            adaptiveCardJson=adaptiveCardJson.Replace("##Confirmed##", data.confirmed.ToString());
            adaptiveCardJson=adaptiveCardJson.Replace("##Active##", data.active.ToString());
            adaptiveCardJson=adaptiveCardJson.Replace("##Recovered##", data.recovered.ToString());
            adaptiveCardJson=adaptiveCardJson.Replace("##Deaths##", data.deaths.ToString());
            var datetime = (new DateTime(1970, 1, 1)).AddMilliseconds(double.Parse(data.last_update.ToString()));
            var d=datetime.ToUniversalTime().ToString();
            var dir = AppDomain.CurrentDomain.BaseDirectory;
            adaptiveCardJson = adaptiveCardJson.Replace("##Last_Update##", d);

            adaptiveCardJson = adaptiveCardJson.Replace("##cDiff##", string.IsNullOrEmpty(model.cDiff.ToString()) ? "0": model.cDiff.ToString());
            adaptiveCardJson = adaptiveCardJson.Replace("##rDiff##", string.IsNullOrEmpty(model.rDiff.ToString()) ? "0" : model.rDiff.ToString());
            adaptiveCardJson = adaptiveCardJson.Replace("##dDiff##", string.IsNullOrEmpty(model.dDiff.ToString()) ? "0" : model.dDiff.ToString());


            var adaptiveCardAttachment = new Attachment()
            {
                ContentType = "application/vnd.microsoft.card.adaptive",
                Content = JsonConvert.DeserializeObject(adaptiveCardJson),
            };
            return adaptiveCardAttachment;
        }
    }
}






