﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AdaptiveCardsBot.Models
{
    public class Covid
    {
        public int id { get; set; }
        public string country { get; set; }
        public int confirmed { get; set; }
        public int active { get; set; }
        public int deaths { get; set; }
        public int recovered { get; set; }
        public string latitude { get; set; }
        public string longitude { get; set; }
        public long last_update { get; set; }
    }
    public class CovidModel
    {
        public int cDiff  { get; set; }
        public int dDiff { get; set; }
        public int rDiff { get; set; }
      
    }
}
